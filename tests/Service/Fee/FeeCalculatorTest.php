<?php

declare(strict_types=1);

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculator;
use PHPUnit\Framework\TestCase;


final class FeeCalculatorTest extends TestCase
{
    /**
     * @dataProvider provideTestRoundInterpolatedResult
     *
     * @param array $provided
     * @param array $expected
     */
    public function testRoundInterpolatedResult(array $provided, array $expected)
    {
        $feeCalculator = new FeeCalculator();
        $result = $feeCalculator->roundInterpolatedResult($provided[0]);
        $this->assertSame($result, $expected[0]);

    }

    public function provideTestRoundInterpolatedResult()
    {
        return [
            [['123.0001'], [125.0]],
            [['0.0001'], [5.0]],
            [['4.9999'], [5.0]],
            [['5.0000'], [5.0]],
            [['5.0001'], [10.0]],
        ];
    }

    /**
     * @dataProvider provideTestGetBounds
     *
     * @param array $provided
     * @param array $expected
     */
    public function testGetBounds(array $provided, array $expected)
    {
        $feeCalculator = new FeeCalculator();
        $bounds = $feeCalculator->getBounds(
            $provided[0],
            $provided[1],
            $provided[2]
        );
        $this->assertSame($expected, $bounds);
    }

    public function provideTestGetBounds()
    {
        return [
            [/** Data set 0 */
                /** provided */
                [ 1500.0, 0, $this->getFees()[0] ],
                /** expected */
                [
                    'upper' => [
                        'fee' => '90',
                        'amount' => '2000',
                    ],
                    'lower' => [
                        'fee' => '50',
                        'amount' => '1000',
                    ],
                ],
            ],

            [/** Data set 1 */
                /** provided */
                [ 2500.0, 0, $this->getFees()[0] ],
                /** expected */
                [
                    'upper' => [
                        'fee' => '90',
                        'amount' => '3000',
                    ],
                    'lower' => [
                        'fee' => '90',
                        'amount' => '2000',
                    ],
                ],
            ],

            [/** Data set 2 */
                /** provided */
                [ 1000.0, 0, $this->getFees()[0] ],
                /** expected */
                [
                    'upper' => [
                        'fee' => '50',
                        'amount' => '1000',
                    ],
                    'lower' => [
                        'fee' => '',
                        'amount' => '',
                    ],
                ],
            ],

            [/** Data set 2 */
                /** provided */
                [ 3000.0, 0, $this->getFees()[0] ],
                /** expected */
                [
                    'upper' => [
                        'fee' => '90',
                        'amount' => '3000',
                    ],
                    'lower' => [
                        'fee' => '90',
                        'amount' => '2000',
                    ],
                ],
            ]
        ];
    }

    /**
     * @dataProvider provideDataToCalculate
     *
     * @param array $provided
     * @param array $expected
     */
    public function testCalculate(array $provided, array $expected)
    {
        $loanApplication = new LoanApplication(
            $provided['term'],
            $provided['amount']
        );
        $feeCalculator = new FeeCalculator();
        $feeCalculator->fees = $provided['fees'];
        $feeCalculator->terms = LoanApplication::TERMS;
        $result = $feeCalculator->calculate($loanApplication);
        $this->assertSame($result, $expected['fee']);
    }

    /**
     * @return array
     */
    public function provideDataToCalculate()
    {
        return [
            [/** Data set 0 */
                [/** provided */
                    'term' => LoanApplication::TERMS[0],
                    'amount' => 20000,
                    'fees' => $this->getFees()[0],
                ],
                [/** expected */
                    'fee' => 400.0
                ]
            ],

            [/** Data set 1 */
                [/** provided */
                    'term' => LoanApplication::TERMS[0],
                    'amount' => 1000,
                    'fees' => $this->getFees()[0],
                ],
                [/** expected */
                    'fee' => 50.0
                ]
            ],

            [/** Data set 2 */
                [/** provided */
                    'term' => LoanApplication::TERMS[0],
                    'amount' => 5500,
                    'fees' => $this->getFees()[0],
                ],
                [/** expected */
                    'fee' => 110.0
                ]
            ],

            [/** Data set 3 */
                [/** provided */
                    'term' => LoanApplication::TERMS[0],
                    'amount' => 5500.01,
                    'fees' => $this->getFees()[0],
                ],
                [/** expected */
                    'fee' => 115.0
                ]
            ],

            [/** Data set 4 */
                [/** provided */
                    'term' => LoanApplication::TERMS[1],
                    'amount' => 2750,
                    'fees' => $this->getFees()[0],
                ],
                [/** expected */
                    'fee' => 115.0
                ]
            ],

            [/** Data set 5 */
                [/** provided */
                    'term' => LoanApplication::TERMS[0],
                    'amount' => 1000,
                    'fees' => $this->getFees()[1],
                ],
                [/** expected */
                    'fee' => 50.0
                ]
            ],

            [/** Data set 6 */
                [/** provided */
                    'term' => LoanApplication::TERMS[1],
                    'amount' => 10000,
                    'terms' => [12, 24],
                    'fees' => $this->getFees()[2],
                ],
                [/** expected */
                    'fee' => 100.0
                ]
            ],

            [/** Data set 7 */
                [/** provided */
                    'term' => LoanApplication::TERMS[1],
                    'amount' => 10000,
                    'terms' => [12, 24],
                    'fees' => $this->getFees()[2],
                ],
                [/** expected */
                    'fee' => 100.0
                ]
            ],

            [/** Data set 8 */
                [/** provided */
                    'term' => LoanApplication::TERMS[1],
                    'amount' => 8499.99,
                    'terms' => [12, 24],
                    'fees' => $this->getFees()[2],
                ],
                [/** expected */
                    'fee' => 85.0
                ]
            ],

            [/** Data set 9 */
                [/** provided */
                    'term' => LoanApplication::TERMS[1],
                    'amount' => 10000,
                    'terms' => LoanApplication::TERMS,
                    'fees' => $this->getFees()[2],
                ],
                [/** expected */
                    'fee' => 100.0
                ]
            ],

            [/** Data set 9 */
                [/** provided */
                    'term' => LoanApplication::TERMS[1],
                    'amount' => 7500,
                    'terms' => LoanApplication::TERMS,
                    'fees' => $this->getFees()[3],
                ],
                [/** expected */
                    'fee' => 150.0
                ]
            ],


        ];
    }

    /**
     * @return array
     */
    public function getFees()
    {
        return [
            0 => LoanApplication::FEES,
            1 => [
                0 => [0, 0],
                2000 => [100, 200],
                4000 => [100, 200],
                8000 => [100, 200],
                16000 => [100, 200],
            ],
            2 => [
                10000 => [10, 100],
            ],
            3 => [
                10000 => [100, 200],
                9000 => [90, 180],
                8000 => [80, 160],
                7000 => [70, 140],
            ]
        ];
    }
}
