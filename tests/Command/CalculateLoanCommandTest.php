<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Tests\Command;

use Lendable\Interview\Interpolation\Command\CalculateLoanCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Tester\CommandTester;

class CalculateLoanCommandTest extends TestCase
{
    /**
     * @dataProvider provideTestExecute
     *
     * @param array $provided
     * @param array $expected
     */
    public function testExecute(array $provided, array $expected)
    {
        $command = new CalculateLoanCommand();

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'term' => $provided[0],
            'amount' => $provided[1]
        ]);

        $output = $commandTester->getDisplay();
        $this->assertEquals($expected[0], (int)$output);
    }

    public function provideTestExecute()
    {
        return [
            [[12, 1499.99], [70]],
            [[12, 1500], [70]],
            [[12, 1500.01], [75]],
            [[12, 1000], [50]],
            [[12, 10000], [200]],
            [[12, 20000], [400]],
            [[12, 4333.33], [115]],
            [[12, 4333.34], [110]],
            [[12, 2222], [90]],
            [[12, 2888], [90]],
            [[24, 1500], [85]],
            [[24, 15500], [620]],
        ];
    }

    /**
     * @dataProvider provideTestTermValidationFailure
     *
     * @param array $provided
     */
    public function testTermValidationFailure(array $provided)
    {
        $command = new CalculateLoanCommand();

        $commandTester = new CommandTester($command);
        $this->expectException(InvalidArgumentException::class);
        $commandTester->execute([
            'term' => $provided[0],
            'amount' => $provided[1]
        ]);
    }

    public function provideTestTermValidationFailure()
    {
        return [
            [[1, 1000]],
            [[-12, 2000]],
            [[-24, 2000]],
            [['string', 3000]],
        ];
    }

    /**
     * @dataProvider provideTestAmountValidationException
     *
     * @param array $provided
     */
    public function testAmountValidationException(array $provided)
    {
        $command = new CalculateLoanCommand();

        $commandTester = new CommandTester($command);
        $this->expectException(InvalidArgumentException::class);
        $commandTester->execute([
            'term' => $provided[0],
            'amount' => $provided[1]
        ]);
    }

    public function provideTestAmountValidationException()
    {
        return [
            [[12, 0]],
            [[12, 20001]],
            [[12, -1]],
            [[12, -000.1]],
            [[12, -20000]],
            [[12, 'string']],
        ];
    }
}
