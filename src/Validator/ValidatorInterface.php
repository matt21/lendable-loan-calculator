<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Validator;

interface ValidatorInterface
{
    /**
     * Executes validation
     *
     * @return bool|array Validation bool true or validation errors array.
     */
    function execute();
}
