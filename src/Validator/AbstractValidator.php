<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Validator;

use Symfony\Component\Console\Exception\InvalidArgumentException;

/**
 * Class AbstractValidator to store and throw validation exceptions
 */
abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * @var array of validation errors
     */
    protected $validationErrors = [];

    /**
     * Checks validationErrors array for errors and throws
     * Exception when it's not empty
     *
     * @return void
     */
    public function execute(): void
    {
        if([] !== $this->validationErrors){
            throw new InvalidArgumentException(
                implode(". \n", $this->validationErrors)
            );
        }
    }
}
