<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Validator\Loan;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Validator\AbstractValidator;
use Respect\Validation\Validator as v;

final class TermsValidator extends AbstractValidator
{
    /**
     * Validates term, if not valid adds error message to validation
     * errors array
     *
     * @param $term
     * @return $this
     */
    public function validateTerm($term): self
    {
        $isTermValid = v::intVal()
            ->in(LoanApplication::TERMS)
            ->validate($term);

        if(!$isTermValid) {
            $this->validationErrors[] = 'Term must be one of these values: ' .
                implode(", ", LoanApplication::TERMS);
        }

        return $this;
    }

    /**
     * Validates amount, if not valid adds error message to validation
     * errors array
     *
     * @param $amount
     * @return $this
     */
    public function validateAmount($amount): self
    {
        $min = min(array_keys(LoanApplication::FEES));
        $max = max(array_keys(LoanApplication::FEES));

        $isAmountValid = v::regex('/^[0-9]+(?:\.[0-9]{0,2})?$/')
            ->between($min, $max)
            ->validate($amount);

        if(!$isAmountValid){
            $this->validationErrors[] =
                "Amount must be a float between {$min} and {$max} and with up to 2 decimal places";
        }

        return $this;
    }
}
