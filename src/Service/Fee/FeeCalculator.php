<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee;

use Lendable\Interview\Interpolation\Model\LoanApplication;

class FeeCalculator implements FeeCalculatorInterface
{
    /**
     * @var array
     */
    public $terms;

    /**
     * @var array
     */
    public $fees;

    /**
     * Sets fees and terms arrays for calculator
     */
    public function __construct()
    {
        $this->terms = LoanApplication::TERMS;
        $this->fees = LoanApplication::FEES;
    }

    /**
     * Calculates the fee for a loan application.
     *
     * @param LoanApplication $application The loan application to
     * calculate for.
     *
     * @return float The calculated fee.
     */
    public function calculate(LoanApplication $application): float
    {
        $term = $application->getTerm();
        $termKey = array_search($term, $this->terms);
        $amount = $application->getAmount();
        $fees = $this->fees;

        if (array_key_exists((int)$amount, $fees)) {
            return (float)$fees[(int)$amount][$termKey];
        }

        $bounds = $this->getBounds($amount, $termKey, $fees);
        return $this->interpolate($bounds, $amount);
    }

    /**
     * Returns array of lower and upper bounds
     *
     * @param float $amount
     * @param int $termKey
     * @param array $fees
     * @return array
     */
    public function getBounds(float $amount, int $termKey, array $fees): array
    {
        reset($fees);
        while (key($fees) < $amount) next($fees);

        return [
            'upper' => [
                'fee' => (string)current($fees)[$termKey],
                'amount' => (string)key($fees),
            ],
            'lower' => [
                'fee' => (string)prev($fees)[$termKey],
                'amount' => (string)key($fees),
            ],

        ];
    }

    /**
     * Interpolates amount between provided bounds
     *
     * @param array $bounds
     * @param float $amount
     * @return float
     */
    public function interpolate(array $bounds, float $amount): float
    {
        bcscale(4);
        $feeRange = bcsub($bounds['upper']['fee'], $bounds['lower']['fee']);
        $amountLowerRange = bcsub((string)$amount, $bounds['lower']['amount']);

        $numerator = bcmul($feeRange, $amountLowerRange);
        $denominator = bcsub($bounds['upper']['amount'], $bounds['lower']['amount']);

        $nonScaledResult = bcdiv($numerator, $denominator);
        $result = bcadd($nonScaledResult, $bounds['lower']['fee']);

        return $this->roundInterpolatedResult($result);
    }

    /**
     * Rounds the result of interpolation
     *
     * @param string $result of interpolation
     * @return float rounded result
     */
    public function roundInterpolatedResult(string $result): float
    {
        $result = bcmul($result, '10000');
        $modulo = bcmod($result, '50000');
        if ('0' !== $modulo){
            $rounded = bcadd(bcsub($result, $modulo), '50000');
        }else{
            $rounded = bcsub($result, $modulo);
        }
        return (float)bcdiv($rounded, '10000');
    }
}
