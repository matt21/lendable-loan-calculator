<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Model;

/**
 * A cut down version of a loan application containing
 * only the required properties for this test.
 */
class LoanApplication
{
    /**
     * Array of available loan terms
     */
    const TERMS = [12, 24];

    /**
     * Array of available fees
     */
    const FEES = [
        1000 =>  [ 50,  70],
        2000 =>  [ 90, 100],
        3000 =>  [ 90, 120],
        4000 =>  [115, 160],
        5000 =>  [100, 200],
        6000 =>  [120, 240],
        7000 =>  [140, 280],
        8000 =>  [160, 320],
        9000 =>  [180, 360],
        10000 => [200, 400],
        11000 => [220, 440],
        12000 => [240, 480],
        13000 => [260, 520],
        14000 => [280, 560],
        15000 => [300, 600],
        16000 => [320, 640],
        17000 => [340, 680],
        18000 => [360, 720],
        19000 => [380, 760],
        20000 => [400, 800],
    ];

    /**
     * @var int
     */
    private $term;

    /**
     * @var float
     */
    private $amount;

    /**
     * @param int $term
     * @param float $amount
     */
    public function __construct(int $term, float $amount)
    {
        $this->term = $term;
        $this->amount = $amount;
    }

    /**
     * Gets the term for this loan application expressed
     * in number of months.
     *
     * @return int
     */
    public function getTerm(): int
    {
        return $this->term;
    }

    /**
     * Gets the amount requested for this loan application.
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
