<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Command;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculator;
use Lendable\Interview\Interpolation\Validator\Loan\TermsValidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateLoanCommand extends Command
{
    protected static $defaultName = 'loan:calculate';

    /**
     * @var FeeCalculator service.
     */
    private $feeCalculator;

    /**
     * Sets a fee calculator property.
     *
     * CalculateLoanCommand constructor.
     */
    public function __construct()
    {
        $this->feeCalculator = new FeeCalculator();
        parent::__construct();
    }

    /**
     * Configures loan:calculate command.
     */
    protected function configure()
    {
        $this
            ->setDescription('Calculates loan fee.')
            ->setHelp(
                'This command calculates loan fee based on term and amount.'
            )
            ->addArgument(
                'term',
                InputArgument::REQUIRED,
                'The term of loan (12 or 24 months)'
            )
            ->addArgument(
                'amount',
                InputArgument::REQUIRED,
                'The amount of loan (1000-20000 pounds)'
            );
    }

    /**
     * Executes a command to calculate loan fee based on term and amount
     * arguments.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $term = $input->getArgument('term');
        $amount = $input->getArgument('amount');

        $validator = new TermsValidator();
        $validator->validateTerm($term)
            ->validateAmount($amount)
            ->execute();

        $application = new LoanApplication((int) $term, (float) $amount);
        $result = $this->feeCalculator->calculate($application);
        $output->writeln($result);
    }
}
