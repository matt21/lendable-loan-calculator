<?php

require __DIR__.'/../vendor/autoload.php';

use Lendable\Interview\Interpolation\Command\CalculateLoanCommand;
use Symfony\Component\Console\Application;

$app = new Application();

$app->add(new CalculateLoanCommand());
$app->setDefaultCommand('loan:calculate', true);

try{
    $app->run();
}catch (Exception $e){
    echo $e->getMessage().' in '.$e->getTraceAsString();
}
